package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Bundle;

public class DetailActivity2 extends AppCompatActivity {

    DatabaseHelper myDb;
    private TextView tvDetailTitle, tvDetailAuthor, tvDetailArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail2);

        setTitle("Detail Article");

        myDb = new DatabaseHelper(this);
        tvDetailTitle = findViewById(R.id.tv_detail_title);
        tvDetailAuthor = findViewById(R.id.tv_detail_author);
        tvDetailArticle = findViewById(R.id.tv_detail_article);
        ImageView toBack = findViewById(R.id.to_back);

        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(DetailActivity2.this, ListActivity.class );
                startActivity(pindah);
            }
        });

        int id = getIntent().getIntExtra("Id", 0);
        Cursor cursor = myDb.getDataById(id);

        cursor.moveToNext();
        tvDetailTitle.setText(cursor.getString(1));
        tvDetailAuthor.setText(cursor.getString(2));
        tvDetailArticle.setText(cursor.getString(3));
    }
}
