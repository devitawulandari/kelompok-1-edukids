package com.example.edukids;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MenuAwalActivity extends AppCompatActivity {

    private ImageView imgBelajar, imgDiari, imgConsole, imgFeedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_awal);

        imgBelajar = findViewById(R.id.belajar);
        imgDiari = findViewById(R.id.diary);
        imgConsole = findViewById(R.id.game);
        imgFeedback = findViewById(R.id.feedback);
        ImageView toProfil = findViewById(R.id.to_profile);

        imgBelajar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(MenuAwalActivity.this, Dashboard.class );
                startActivity(pindah);
            }
        });

        imgDiari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(MenuAwalActivity.this, Main3Activity.class );
                startActivity(pindah);
            }
        });

        imgConsole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(MenuAwalActivity.this, KuisActivity.class );
                startActivity(pindah);
            }
        });

        imgFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(MenuAwalActivity.this, FeedbackActivity.class );
                startActivity(pindah);
            }
        });

        toProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(MenuAwalActivity.this, Main2Activity.class );
                startActivity(pindah);
            }
        });
    }


    }


