package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class binatangActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binatang);

        final ImageView ImgAyam =  findViewById(R.id.ayamImg);
        final ImageView ImgBabi =  findViewById(R.id.babiImg);
        final ImageView ImgBebek =  findViewById(R.id.bebekImg);
        final ImageView ImgRino =  findViewById(R.id.rinoImg);
        final ImageView ImgSapi =  findViewById(R.id.sapiImg);
        final ImageView ImgUdang =  findViewById(R.id.udangImg);
        final ImageView ImgElang =  findViewById(R.id.elangImg);
        final ImageView ImgKepiting =  findViewById(R.id.kepitingImg);
        final ImageView ImgLobster =  findViewById(R.id.lobsterImg);
        final ImageView ImgKucing =  findViewById(R.id.kucingImg);
        ImageView toProfil = findViewById(R.id.to_profile);
        ImageView toBack = findViewById(R.id.to_back);


        ImgAyam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.ayam);
                pindah.putExtra("nama", "A Y A M");
                pindah.putExtra("ejaan","A - Y - A - M");
                pindah.putExtra("info","Ayam peliharaan adalah unggas yang biasa dipelihara orang untuk dimanfaatkan untuk keperluan hidup pemeliharanya.");
                startActivity(pindah);
            }
        });

        ImgBabi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.babi);
                pindah.putExtra("nama", "B A B I");
                pindah.putExtra("ejaan","B - A - B - I");
                pindah.putExtra("info","Babi adalah sejenis hewan ungulata yang bermoncong panjang dan berhidung lemper dan merupakan hewan yang aslinya berasal dari Eurasia");
                startActivity(pindah);
            }
        });
        ImgBebek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.bebek);
                pindah.putExtra("nama", "B E B E K");
                pindah.putExtra("ejaan","B - E - B - E - K");
                pindah.putExtra("info","Bebek adalah burung akuatik yang sebagian besar berukuran lebih kecil dibandingkan kerabatnya, dan dapat ditemukan pada perairan air tawar maupun air laut");
                startActivity(pindah);
            }
        });
        ImgRino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.badak);
                pindah.putExtra("nama", "B A D A K");
                pindah.putExtra("ejaan","B - A - D - A - K");
                pindah.putExtra("info","Badak adalah lima spesies hewan dari famili Rhinocerotidae, ordo Perissodactyla yang kesemuanya berasal dari Afrika atau Asia.");
                startActivity(pindah);
            }
        });
        ImgSapi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.sapi);
                pindah.putExtra("nama", "S A P I");
                pindah.putExtra("ejaan","S - A - P - I");
                pindah.putExtra("info","Sapi adalah hewan ternak anggota suku Bovidae dan anak suku Bovinae. Sapi yang telah dikebiri dan biasanya digunakan untuk membajak sawah dinamakan lembu");
                startActivity(pindah);
            }
        });
        ImgUdang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.udang);
                pindah.putExtra("nama", "U D A N G");
                pindah.putExtra("ejaan","U - D - A - N - G");
                pindah.putExtra("info","Udang adalah binatang yang hidup di perairan, khususnya sungai, laut, atau danau.");
                startActivity(pindah);
            }
        });
        ImgElang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.elang);
                pindah.putExtra("nama", "E L A N G");
                pindah.putExtra("ejaan","E - L - A - N - G");
                pindah.putExtra("info","Elang adalah hewan berdarah panas, mempunyai sayap dan tubuh yang diselubungi bulu pelepah");
                startActivity(pindah);
            }
        });
        ImgKepiting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.kepiting);
                pindah.putExtra("nama", "K E P I T I N G");
                pindah.putExtra("ejaan","K - E - P - I - T - I - N - G");
                pindah.putExtra("info","Kepiting adalah binatang anggota krustasea berkaki sepuluh dari upabangsa Brachyura, yang dikenal mempunyai \"ekor\" yang sangat pendek");
                startActivity(pindah);
            }
        });
        ImgLobster.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.lobster);
                pindah.putExtra("nama", "L O B S T E R");
                pindah.putExtra("ejaan","L - O - B - S - T - E - R");
                pindah.putExtra("info","Lobster air tawar adalah crustacea yang menyerupai lobster dan hidup di air tawar yang tidak dapat membeku sampai ke dasar.");
                startActivity(pindah);
            }
        });
        ImgKucing.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.kucing);
                pindah.putExtra("nama", "K U C I N G");
                pindah.putExtra("ejaan","K - U - C - I - N - G");
                pindah.putExtra("info","Kucing disebut juga kucing domestik atau kucing rumah adalah sejenis mamalia karnivora dari keluarga Felidae.");
                startActivity(pindah);
            }
        });

        toProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, Main2Activity.class );
                startActivity(pindah);
            }
        });

        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(binatangActivity.this, Dashboard.class );
                startActivity(pindah);
            }
        });
    }
}
