package com.example.edukids;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.UUID;

public class DataActivity extends AppCompatActivity {

    private Button btn_up, btn_ch, btn_c;
    private ImageView imageView;
    private Uri filePath;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private EditText Fullname, Account, Phon, Pass, address;
    //private FirebaseAuth mFirebaseAuth;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        btn_up = (Button) findViewById(R.id.btn_upload);
        btn_ch = (Button) findViewById(R.id.btn_choose);
        imageView = (ImageView) findViewById(R.id.profile_image);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        btn_c = (Button) findViewById(R.id.btn_change);
        Fullname = (EditText) findViewById(R.id.Full);
        Phon = (EditText) findViewById(R.id.Phone);
        address = (EditText) findViewById(R.id.addr);

        databaseReference = FirebaseDatabase.getInstance().getReference("Users");
//        mFirebaseAuth =FirebaseAuth.getInstance();


        btn_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                CreateAlertDialoge();
                addUser();
            }
        });

        btn_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                uploadImage();
            }
        });
        btn_ch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                chooseImage();

            }
        });
    }
    private void CreateAlertDialoge() {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage("Change Your Profile?");
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(DataActivity.this, "Yes",Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(DataActivity.this, "",Toast.LENGTH_SHORT).show();
            }
        });
        builder.create();
        builder.show();
    }


    private void addUser() {
        String fullname = Fullname.getText().toString();
        String nomor = Phon.getText().toString();
        String alamat = address.getText().toString();

        if (!TextUtils.isEmpty(fullname)) {
            String id = databaseReference.push().getKey();
            User user = new User(fullname, nomor, alamat);
            databaseReference.child(id).setValue(user);

            Toast.makeText(this, "Please Enter Fullname", Toast.LENGTH_LONG).show();
        }

        if (TextUtils.isEmpty(nomor)) {
            Toast.makeText(DataActivity.this, "Please Enter Phone Number", Toast.LENGTH_SHORT).show();
        }

        if (TextUtils.isEmpty(alamat)) {
            Toast.makeText(DataActivity.this, "Please Enter Address", Toast.LENGTH_SHORT).show();
        }

    }





    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), 1);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadImage() {
        if (filePath!=null){

            final ProgressDialog progressDialog =new ProgressDialog(this);
            progressDialog.setTitle("Uploading....");
            progressDialog.show();
            StorageReference reference =storageReference.child("images/" + UUID.randomUUID().toString());

            reference.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(DataActivity.this, "Image Upload", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            double progres = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            progressDialog.setMessage("Uploaded"+ (int)progres+"%");
                        }
                    });
        }



    }
}
