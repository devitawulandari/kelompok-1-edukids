package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import android.os.Bundle;

public class CreateActivity extends AppCompatActivity {

    DatabaseHelper myDb;
    private EditText etTitle, etAuthor, etArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        setTitle("Create Article");

        myDb = new DatabaseHelper(this);
        etTitle = findViewById(R.id.edt_input_title);
        etAuthor = findViewById(R.id.edt_input_author);
        etArticle = findViewById(R.id.edt_input_article);
        ImageView toBack = findViewById(R.id.to_back);

        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(CreateActivity.this, Main3Activity.class );
                startActivity(pindah);
            }
        });
    }


    public void clickPost(View view) {
        String title = etTitle.getText().toString();
        String author = etAuthor.getText().toString();
        String article = etArticle.getText().toString();

        if (title.equals("") || author.equals("") || article.equals("")) {
            Toast.makeText(this, "Mohon isi data secara lengkap", Toast.LENGTH_SHORT).show();
        } else {
            boolean insert = myDb.insertData(title, author, article);
            if (insert){
                etTitle.setText("");
                etAuthor.setText("");
                etArticle.setText("");
                Toast.makeText(this, "Artikel berhasil di tambahkan", Toast.LENGTH_SHORT).show();
            } else{
                Toast.makeText(this, "Artikel gagal di tambahkan", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

