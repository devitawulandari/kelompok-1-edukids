package com.example.edukids;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.edukids.FotoActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static final String TAG = "profileFragment";

    TextView userEmail,textView;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    ImageView keluar,Setting;
    ImageView ImgProfil;
    private LinearLayout linearLayout, linearLayout1;
    int mColor,mAA;

    int warna,warna1;

    // Key for current color
    private final String COLOR_KEY = "color";

    // Shared preferences object
    private SharedPreferences mPreferences;
    // Name of shared preferences file

    SharedPreferences.Editor preferencesEditor;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        Button Kembali = view.findViewById(R.id.keluar_balik);
        Setting = view.findViewById(R.id.ImgSetting);
        userEmail = view.findViewById(R.id.tvEmail);
        linearLayout = view.findViewById(R.id.parentLy);
        linearLayout1 = view.findViewById(R.id.toolbarLy);
        textView = view.findViewById(R.id.textView4);
        firebaseAuth = firebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        ImgProfil = view.findViewById(R.id.profile_image);

        userEmail.setText(firebaseUser.getEmail());

        mPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        mColor = mPreferences.getInt("background", 0);
        mAA = mColor;

        if (mAA != 0){
            linearLayout.setBackgroundColor(getResources().getColor(R.color.hitam));
            linearLayout1.setBackgroundColor(getResources().getColor(R.color.hitam));
            textView.setBackgroundResource(R.drawable.biru);
            userEmail.setTextColor(getResources().getColor(R.color.white));
            Setting.setImageResource(R.drawable.ic_settings_white_24dp);
        } else {
            textView.setBackgroundResource(R.drawable.biru);
            linearLayout.setBackgroundColor(getResources().getColor(R.color.white));
            linearLayout1.setBackgroundColor(getResources().getColor(R.color.biru));
            userEmail.setTextColor(getResources().getColor(R.color.hitam));
            Kembali.setBackgroundColor(getResources().getColor(R.color.biru));
            Setting.setImageResource(R.drawable.set);
        }


        Kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(requireContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        ImgProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(requireContext(), FotoActivity.class);
                startActivity(intent);
            }
        });


        Setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(requireContext(), SettingActivity.class );
                startActivity(pindah);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        mColor = mPreferences.getInt("background", 0);
        mAA = mColor;

        if (mAA != 0){
            linearLayout.setBackgroundColor(getResources().getColor(R.color.hitam));
            linearLayout1.setBackgroundColor(getResources().getColor(R.color.biru));
            textView.setBackgroundResource(R.drawable.biru);
            userEmail.setTextColor(getResources().getColor(R.color.white));
            Setting.setImageResource(R.drawable.set);
        } else {
            textView.setBackgroundResource(R.drawable.biru);
            linearLayout.setBackgroundColor(getResources().getColor(R.color.white));
            linearLayout1.setBackgroundColor(getResources().getColor(R.color.biru));
            userEmail.setTextColor(getResources().getColor(R.color.hitam));
            Setting.setImageResource(R.drawable.set);
        }


    }
}
