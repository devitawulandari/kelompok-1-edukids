package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class HurufActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huruf);

        final TextView EtHurufa =  findViewById(R.id.hurufa);
        final TextView EtHurufb =  findViewById(R.id.hurufb);
        final TextView EtHurufc =  findViewById(R.id.hurufc);
        final TextView EtHurufd =  findViewById(R.id.hurufd);
        final TextView EtHurufe =  findViewById(R.id.hurufe);
        final TextView EtHuruff =  findViewById(R.id.huruff);
        final TextView EtHurufg =  findViewById(R.id.hurufg);
        final TextView EtHurufh =  findViewById(R.id.hurufh);
        final TextView EtHurufi =  findViewById(R.id.hurufi);
        final TextView EtHurufj =  findViewById(R.id.hurufj);
        final TextView EtHurufk =  findViewById(R.id.hurufk);
        final TextView EtHurufl =  findViewById(R.id.hurufl);
        final TextView EtHurufm =  findViewById(R.id.hurufm);
        final TextView EtHurufn =  findViewById(R.id.hurufn);
        final TextView EtHurufo =  findViewById(R.id.hurufo);
        final TextView EtHurufp =  findViewById(R.id.hurufp);
        final TextView EtHurufq =  findViewById(R.id.hurufq);
        final TextView EtHurufr =  findViewById(R.id.hurufr);
        final TextView EtHurufs =  findViewById(R.id.hurufs);
        final TextView EtHuruft =  findViewById(R.id.huruft);
        final TextView EtHurufu =  findViewById(R.id.hurufu);
        final TextView EtHurufv =  findViewById(R.id.hurufv);
        final TextView EtHurufw =  findViewById(R.id.hurufw);
        final TextView EtHurufx=   findViewById(R.id.hurufx);
        final TextView EtHurufy =  findViewById(R.id.hurufy);
        final TextView EtHurufz =  findViewById(R.id.hurufz);
        ImageView toProfil = findViewById(R.id.to_profile);
        ImageView toBack = findViewById(R.id.to_back);




        EtHurufa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("alphabet","A");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "A (INDONESIAN)");
                pindah.putExtra("ejaan","E-I (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","B");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "B (INDONESIAN)");
                pindah.putExtra("ejaan","B-I (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","C");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "C (INDONESIAN)");
                pindah.putExtra("ejaan","S-I (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","D");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "D (INDONESIAN)");
                pindah.putExtra("ejaan","D-I (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","E");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "E (INDONESIAN)");
                pindah.putExtra("ejaan","I (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHuruff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","F");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "F (INDONESIAN)");
                pindah.putExtra("ejaan","E-F (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","G");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "G (INDONESIAN)");
                pindah.putExtra("ejaan","J-I (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","H");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "H (INDONESIAN)");
                pindah.putExtra("ejaan","E-I-J (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","I");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "I (INDONESIAN)");
                pindah.putExtra("ejaan","A-I (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","J");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "J (INDONESIAN)");
                pindah.putExtra("ejaan","J-E-Y (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","K");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "K (INDONESIAN)");
                pindah.putExtra("ejaan","K-E-Y (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","L");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "L (INDONESIAN)");
                pindah.putExtra("ejaan","E-L (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","M");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "M (INDONESIAN)");
                pindah.putExtra("ejaan","E-M (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","N");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "F (INDONESIAN)");
                pindah.putExtra("ejaan","E-N (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","O");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "O (INDONESIAN)");
                pindah.putExtra("ejaan","O-U (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","P");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "P (INDONESIAN)");
                pindah.putExtra("ejaan","P-I (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","Q");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "Q (INDONESIAN)");
                pindah.putExtra("ejaan","K-Y-U (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","R");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "R (INDONESIAN)");
                pindah.putExtra("ejaan","A-R-R (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","S");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "S (INDONESIAN)");
                pindah.putExtra("ejaan","E-S (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHuruft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","T");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "T (INDONESIAN)");
                pindah.putExtra("ejaan","T-I (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","U");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "U (INDONESIAN)");
                pindah.putExtra("ejaan","Y-U (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","V");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "V (INDONESIAN)");
                pindah.putExtra("ejaan","V-I (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","W");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "W (INDONESIAN)");
                pindah.putExtra("ejaan","D-A-B-E-L-Y-U (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","X");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "X (INDONESIAN)");
                pindah.putExtra("ejaan","E-K-S (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","Y");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "Y (INDONESIAN)");
                pindah.putExtra("ejaan","W-A-Y (ENGLISH)");
                startActivity(pindah);
            }
        });

        EtHurufz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","Z");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "Z (INDONESIAN)");
                pindah.putExtra("ejaan","Z-I (ENGLISH)");
                startActivity(pindah);
            }
        });

        toProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, Main2Activity.class );
                startActivity(pindah);
            }
        });

        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, Dashboard.class );
                startActivity(pindah);
            }
        });


    }
}
