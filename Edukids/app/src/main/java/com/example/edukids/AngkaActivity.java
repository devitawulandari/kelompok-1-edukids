package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class AngkaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angka);

        final TextView EtAngka0 =  findViewById(R.id.angka0);
        final TextView EtAngka1 =  findViewById(R.id.angka1);
        final TextView EtAngka2 =  findViewById(R.id.angka2);
        final TextView EtAngka3 =  findViewById(R.id.angka3);
        final TextView EtAngka4 =  findViewById(R.id.angka4);
        final TextView EtAngka5 =  findViewById(R.id.angka5);
        final TextView EtAngka6 =  findViewById(R.id.angka6);
        final TextView EtAngka7 =  findViewById(R.id.angka7);
        final TextView EtAngka8 =  findViewById(R.id.angka8);
        final TextView EtAngka9 =  findViewById(R.id.angka9);
        final TextView EtAngka10 =  findViewById(R.id.angka10);
        ImageView toProfil = findViewById(R.id.to_profile);
        ImageView toBack = findViewById(R.id.to_back);

        EtAngka0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",0);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "K O S O N G");
                pindah.putExtra("ejaan","Z E R O");
                pindah.putExtra("info","0 = Kosong");
                startActivity(pindah);
            }
        });
        EtAngka1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",1);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "S A T U");
                pindah.putExtra("ejaan","O N E");
                pindah.putExtra("info"," 1 = Satu \n 10 = Sepuluh \n 100 = Seratus \n 1.000 = Seribu \n 10.000 = Sepuluh Ribu" +
                        "\n 100.000 = Seratus Ribu \n 1.000.000 = Satu Juta \n 10.000.000 = Sepuluh Juta \n 100.000.000 = Seratus Juta" +
                        " \n 1.000.000.000 = Satu Milyar \n 10.000.000.000 = Sepuluh Milyar \n 100.000.000.000 = Seratus Milyar" +
                        "\n 1.000.000.000.000 = Satu Triliun");
                startActivity(pindah);
            }
        });
        EtAngka2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",2);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "D U A");
                pindah.putExtra("ejaan","T W O");
                pindah.putExtra("info"," 2 = Dua \n 20 = Dua Puluh \n 200 = Dua Ratus \n 2.000 = Dua Ribu \n 20.000 = Dua puluh Ribu" +
                        "\n 200.000 = Dua ratus Ribu \n 2.000.000 = Dua Juta \n 20.000.000 = Dua Puluh Juta \n 200.000.000 = Dua Ratus Juta" +
                        " \n 2.000.000.000 = Dua Milyar \n 20.000.000.000 = Dua Puluh Milyar \n 200.000.000.000 = Dua Ratus Milyar" +
                        "\n 2.000.000.000.000 = Dua Triliun");
                startActivity(pindah);
            }
        });

        EtAngka3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",3);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "T I G A");
                pindah.putExtra("ejaan","T H R E E");
                pindah.putExtra("info"," 3 = Tiga \n 30 = Tiga Puluh \n 300 = Tiga Ratus \n 3.000 = Tiga Ribu \n 30.000 = Tiga puluh Ribu" +
                        "\n 300.000 = Tiga ratus Ribu \n 3.000.000 = Tiga Juta \n 30.000.000 = Tiga Puluh Juta \n 300.000.000 = Tiga Ratus Juta" +
                        " \n 3.000.000.000 = Tiga Milyar \n 30.000.000.000 = Tiga Puluh Milyar \n 300.000.000.000 = Tiga Ratus Milyar" +
                        "\n 3.000.000.000.000 = Tiga Triliun");
                startActivity(pindah);
            }
        });

        EtAngka4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",4);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "E M P A T");
                pindah.putExtra("ejaan","F O U R");
                pindah.putExtra("info"," 4 = Empat \n 40 = Empat Puluh \n 400 = Empat Ratus \n 4.000 = Empat Ribu \n 40.000 = Empat puluh Ribu" +
                        "\n 400.000 = Empat ratus Ribu \n 4.000.000 = Empat Juta \n 40.000.000 = Empat Puluh Juta \n 400.000.000 = Empat Ratus Juta" +
                        " \n 4.000.000.000 = Empat Milyar \n 40.000.000.000 = Empat Puluh Milyar \n 400.000.000.000 = Empat Ratus Milyar" +
                        "\n 4.000.000.000.000 = Empat Triliun");
                startActivity(pindah);
            }
        });

        EtAngka5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",5);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "L I M A");
                pindah.putExtra("ejaan","F I V E");
                pindah.putExtra("info"," 5 = Lima \n 50 = Lima Puluh \n 500 = Lima Ratus \n 4.000 = Lima Ribu \n 50.000 = Lima puluh Ribu" +
                        "\n 500.000 = Lima ratus Ribu \n 5.000.000 = Lima Juta \n 50.000.000 = Lima Puluh Juta \n 500.000.000 = Lima Ratus Juta" +
                        " \n 5.000.000.000 = Lima Milyar \n 50.000.000.000 = Lima Puluh Milyar \n 500.000.000.000 = Lima Ratus Milyar" +
                        "\n 5.000.000.000.000 = Lima Triliun");
                startActivity(pindah);
            }
        });

        EtAngka6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",6);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "E N A M");
                pindah.putExtra("ejaan","S I X");
                pindah.putExtra("info"," 6 = Enam \n 60 = Enam Puluh \n 600 = Enam Ratus \n 6.000 = Enam Ribu \n 60.000 = Enam puluh Ribu" +
                        "\n 600.000 = Enam ratus Ribu \n 6.000.000 = Enam Juta \n 60.000.000 = Enam Puluh Juta \n 600.000.000 = Enam Ratus Juta" +
                        " \n 6.000.000.000 = 6nam Milyar \n 60.000.000.000 = Enam Puluh Milyar \n 600.000.000.000 = 6nam Ratus Milyar" +
                        "\n 6.000.000.000.000 = Enam Triliun");
                startActivity(pindah);
            }
        });

        EtAngka7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",7);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "T U J U H");
                pindah.putExtra("ejaan","S E V E N");
                pindah.putExtra("info"," 7 = Tujuh \n 70 = Tujuh Puluh \n 700 = Tujuh Ratus \n 7.000 = Tujuh Ribu \n 70.000 = Tujuh puluh Ribu" +
                        "\n 700.000 = Tujuh ratus Ribu \n 7.000.000 = Tujuh Juta \n 70.000.000 = Tujuh Puluh Juta \n 700.000.000 = Tujuh Ratus Juta" +
                        " \n 7.000.000.000 = Tujuh Milyar \n 70.000.000.000 = Tujuh Puluh Milyar \n 700.000.000.000 = Tujuh Ratus Milyar" +
                        "\n 7.000.000.000.000 = Tujuh Triliun");
                startActivity(pindah);
            }
        });

        EtAngka8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",8);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "D E L A P A N");
                pindah.putExtra("ejaan","E I G H T");
                pindah.putExtra("info"," 8 = Delapan \n 80 = Delapan Puluh \n 800 = Delapan Ratus \n 8.000 = Delapan Ribu \n 80.000 = Delapan puluh Ribu" +
                        "\n 800.000 = Delapan ratus Ribu \n 8.000.000 = Delapan Juta \n 80.000.000 = Delapan Puluh Juta \n 800.000.000 = Delapan Ratus Juta" +
                        " \n 8.000.000.000 = Delapan Milyar \n 80.000.000.000 = Delapan Puluh Milyar \n 800.000.000.000 = Delapan Ratus Milyar" +
                        "\n 8.000.000.000.000 = Delapan Triliun");
                startActivity(pindah);
            }
        });

        EtAngka9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",9);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "S E M B I L A N");
                pindah.putExtra("ejaan","N I N E");
                pindah.putExtra("info"," 9 = Sembilan \n 90 = Sembilan Puluh \n 900 = Sembilan Ratus \n 9.000 = Sembilan Ribu \n 90.000 = Sembilan puluh Ribu" +
                        "\n 900.000 = Sembilan ratus Ribu \n 9.000.000 = Sembilan Juta \n 90.000.000 = Sembilan Puluh Juta \n 900.000.000 = Sembilan Ratus Juta" +
                        " \n 9.000.000.000 = Sembilan Milyar \n 90.000.000.000 = Sembilan Puluh Milyar \n 900.000.000.000 = Sembilan Ratus Milyar" +
                        "\n 9.000.000.000.000 = Sembilan Triliun");
                startActivity(pindah);
            }
        });

        EtAngka10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, DetailActivity.class );
                pindah.putExtra("alpha",10);
                pindah.putExtra("untuk","angka");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "S E P U L U H");
                pindah.putExtra("ejaan","T E N");
                pindah.putExtra("info"," 10 = Sepuluh \n 10.000 = Sepuluh Ribu +\n" +
                        "                        \n 10.000.000 = Sepuluh Juta  +\n" +
                        "                        \n 10.000.000.000 = Sepuluh Milyar  +\n" +
                        "                        ");
                startActivity(pindah);
            }
        });

        toProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, Main2Activity.class );
                startActivity(pindah);
            }
        });

        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(AngkaActivity.this, Dashboard.class );
                startActivity(pindah);
            }
        });


    }
}


