package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class sayurActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sayur);

        final ImageView ImgBrokoli =  findViewById(R.id.brokoliImg);
        final ImageView ImgBuncis =  findViewById(R.id.buncisImg);
        final ImageView ImgKol =  findViewById(R.id.kolImg);
        final ImageView ImgTerong =  findViewById(R.id.terongImg);
        final ImageView ImgSawi =  findViewById(R.id.sawiImg);
        final ImageView ImgWortel =  findViewById(R.id.wortelImg);
        final ImageView ImgToge =  findViewById(R.id.togeImg);
        final ImageView ImgTomat =  findViewById(R.id.tomatImg);
        ImageView toProfil = findViewById(R.id.to_profile);
        ImageView toBack = findViewById(R.id.to_back);



        ImgBrokoli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(sayurActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.brokoli);
                pindah.putExtra("nama", "B R O K O L I");
                pindah.putExtra("ejaan","B - R - O - K - O - L - I");
                pindah.putExtra("info"," Brokoli berasal dari keluarga kubis dan dikategorikan sebagai \n" +
                        "tanaman hijau yang dapat dimakan, juga dikenal sebagai sayuran hangat dan lezat \n" +
                        "yang kaya dengan puluhan nutrisi dari setiap sayuran.\n" +
                        "Sayuran hijau ini merupakan sayuran tanpa lemak yang rendah sodium, \n" +
                        "kalori dan tinggi akan serat, vitamin C, potasium, B6 dan Vitamin A.");
                startActivity(pindah);
            }
        });

        ImgBuncis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(sayurActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.buncis);
                pindah.putExtra("nama", "B U N C I S");
                pindah.putExtra("ejaan","B - U - N - C - I - S");
                pindah.putExtra("info","Buncis dalam bahasa Inggris yaitu green beans atau snap beans. Sayuran ini memiliki \n" +
                        "banyak sekali manfaat yaitu dapat membantu menurukan berat badan, baik untuk kesehatan jantung,\n" +
                        "dapat mencegah kanker, mencegah depresi, dll. ");
                startActivity(pindah);
            }
        });

        ImgKol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(sayurActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.kol);
                pindah.putExtra("nama", "K O L");
                pindah.putExtra("ejaan","K - O - L");
                pindah.putExtra("info","Kubis atau kol merupakan salah satu jenis sayuran yang cukup akrab di lidah masyarakat Indonesia. Memiliki tekstur renyah dan rasa yang ringan. \n" +
                        "Karena itulah, sayuran yang memiliki nama ilmiah Brassica oleracea var. capitata ini sering diolah menjadi beragam hidangan lezat; seperti sup, bakwan, hingga siomay.\n" +
                        "Jenis kol juga beragam. Terdapat kol hijau, kol ungu, kol napa, kol savoy, dll.\n");
                startActivity(pindah);
            }
        });

        ImgTerong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(sayurActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.terong);
                pindah.putExtra("nama", "T E R O N G");
                pindah.putExtra("ejaan","T - E - R - O - N - G");
                pindah.putExtra("info","Terong merupakan sayuran yang harganya cukup terjangkau dan walaupun terjangkau terong ini sangat bermanfaat bagi tubu karena gizi - gizi yang dikandungnya \n" +
                        "cukup banyak sehingga cocok dikonsumsi oleh anak - anak didalam masa pertumbuhan. Selain itu manfaat terong juga banyak seperti baik untuk jantung, baik untuk otak, \n" +
                        "baik untuk kulit, dll.");
                startActivity(pindah);
            }
        });

        ImgSawi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(sayurActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.sawi);
                pindah.putExtra("nama", "S A W I");
                pindah.putExtra("ejaan","S - A - W - I");
                pindah.putExtra("info","Sawi merupakan sayuran yang berasal dari marga Brassica. Jenis sayuran ini bermacam-macam, mulai dari sawi hijau seperti caisim dan pakcoy, sawi pahit atau mustard greens, hingga sawi putih.\n" +
                        "Banyak vitamin yang terkandung dalam sawi seperti  vitamin B kompleks dalam bentuk asam folat, vitamin A yang berasal dari karoten, vitamin C, dan vitamin K.");
                startActivity(pindah);
            }
        });

        ImgToge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(sayurActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.toge);
                pindah.putExtra("nama", "T O G E");
                pindah.putExtra("ejaan","T - O - G - E");
                pindah.putExtra("info","Toge merupakan sayuran yang banyak dikonsumsi masyarakat Indonesia. Salah satu manfaat toge yang paling umum adalah meringankan kerja sistem pencernaan dalam tubuh, sekaligus membantu penyerapan zat besi, \n" +
                        "vitamin C, dan zinc yang terkandung di dalam toge. Itu sebabnya toge bisa menjadi pilihan tepat untuk yang mengalami gangguan pencernaan, ataupun yang memiliki reaksi hipersensitivitas terhadap biji-bijian.");
                startActivity(pindah);
            }
        });

        ImgWortel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(sayurActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.wortel);
                pindah.putExtra("nama", "W O R T E L");
                pindah.putExtra("ejaan","W - O - R - T - E - L");
                pindah.putExtra("info"," Wortel merupakan sayuran sarat vitamin yang mengandung vitamin A, vitamin B, vitamin C,  vitamin E, serta vitamin K. \n" +
                        "Sayuran berwarna jingga ini merupakan sumber karotenoid atau beta karoten, kalium, serat, serta antioksidan yang dibutuhkan oleh tubuh.");
                startActivity(pindah);
            }
        });

        ImgTomat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(sayurActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.tomat);
                pindah.putExtra("nama", "T O M A T");
                pindah.putExtra("ejaan","T - O - M - A - T");
                pindah.putExtra("info","Tomat termasuk kedalam jenis buah dan sayur, karena dalam dunia kuliner tomat dikategorikan sebagai sayur-sayuran.\n" +
                        "Dan biasanya juga tomat digunakan untuk membuat masakan asin seperti sup atau untuk menumis.");
                startActivity(pindah);
            }
        });

        toProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(sayurActivity.this, Main2Activity.class );
                startActivity(pindah);
            }
        });

        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(sayurActivity.this, Dashboard.class );
                startActivity(pindah);
            }
        });

    }
}
