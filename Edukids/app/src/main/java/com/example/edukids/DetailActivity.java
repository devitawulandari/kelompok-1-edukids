package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        TextView tvBinatang =  findViewById(R.id.tv_Binatang);
        ImageView tvGambar = findViewById(R.id.tv_gambar);
        TextView tvNama =  findViewById(R.id.tv_nama);
        TextView tvEjaan =  findViewById(R.id.tv_ejaan);

        tvBinatang.setText(getIntent().getStringExtra("judul"));
        tvGambar.setImageResource(getIntent().getIntExtra("gambar",0));
        tvNama.setText(getIntent().getStringExtra("nama"));
        tvEjaan.setText(getIntent().getStringExtra("ejaan"));

    }
}
